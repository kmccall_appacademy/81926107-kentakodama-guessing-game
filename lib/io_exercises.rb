# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game
  number = rand(100) + 1
  count = 0
  guess = nil
  guesses = ''
  until guess == number
    p "guess a number"
    guess = gets.chomp.to_i
    feedback(number, guess)
    guesses += "#{guess.to_s}\n"
    p guesses
    count += 1
  end

  p "Correct number: #{number}. You guessed #{count} times."

end

def feedback(num, guess)

  p 'too high' if guess > num
  p 'too low' if guess < num

end




# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
